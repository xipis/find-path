# 简单的寻路算法可视化示例
<br/>
<br/>

### 运行示例 
![image](https://gitee.com/jiankesword/image-layer/raw/master/searchPath/search.gif)

### 项目结构
![image](https://gitee.com/jiankesword/image-layer/raw/master/searchPath/structure.png)

### 寻路算法
网上很多文章，这里就不重复介绍了  
如果看了网上的文章仍然觉得A*寻路难以理解可以运行本项目观察寻路过程  
建议使用单步寻路功能，观察程序是如何一步一步从起点找到终点的  

### 使用方法
 > 使用 cocos creator 2.1.0 打开本项目  
 > 在 chome 浏览器中运行   
 > 调试： 编辑语言 javaScript   
const startGrid = 'STAR_GRID'
const endGrid = 'END_GRID'
const normalGrid = 'NORMAL_GRID'

export { startGrid, endGrid, normalGrid }
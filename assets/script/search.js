import { startGrid, endGrid, normalGrid } from './constant'

class Search {
    constructor(Grid) {
        this.openList = [] // 记录待检查的列表 元素类型是block
        this.closeList = [] // 不再检查的列表
        this.results = []    // 结果路径格子数组
        this.pathPoint = [] // 可以绘制的路径点
        this.grid = Grid    // 传入的地图对象
        this.searchComplete = false // 检索完成
        this.searchInit()
    }

    // 将起点加入open列表，修改过起点后应该执行
    searchInit() {
        let startBlock = this.grid.gridToBlock(this.grid.startBlock)
        this.openList = [startBlock]
    }

    // 检查这个块的类型
    checkType(block) {
        return this.grid.checkGridType(this.grid.blockToGrid(block))
    }

    // 寻路的核心步骤
    stepSearch() {
        let currentBlock = this.openList.pop()
        let type = this.checkType(currentBlock)
        if (type != startGrid && type != endGrid) {
            this.grid.changeToClose(currentBlock)
        }
        this.closeList.push(currentBlock)
        let neighborArray = this.getNeighborBlock(currentBlock)
        let newOpenNeighbor = this.neighborProcess(neighborArray, currentBlock)

        // 合并数组
        this.openList = [...this.openList, ...newOpenNeighbor]

        // pop操作选择F值最小的  所以根据F值从大到小排序 再根据G进行二级排序
        // 排序方法不同，面对不同地形的搜索效率也不同，感兴趣可以修改一下排序方法
        this.openList.sort((a, b) => {
            if (b.F == a.F) {
                return a.H - b.H // 仍然有可能不是最短路径，较优路径但是搜索的次数大幅增长
                // return a.G - b.G  // 不是最短路径，搜索比较快
            }
            return b.F - a.F
        })

        // 查看是否已经搜索到终点
        let end = this.grid.endBlock
        neighborArray.map(i => {
            let g = this.grid.blockToGrid(i)
            if (g.x == end.x && g.y == end.y) {
                console.log('找到终点了')
                this.searchComplete = true
                let endBlock = this.grid.gridToBlock(end)
                endBlock.parent = currentBlock
                this.parseResult(endBlock)
            }
        })
    }

    // 递归 解析路径
    parseResult(block) {
        if (block.parent) {
            this.results.push(block)
            this.parseResult(block.parent)
        } else if (block.parent == null) {
            let startBlock = this.grid.gridToBlock(this.grid.startBlock)
            this.results.push(startBlock)
            // console.log(this.results)
            this.parsePath()
            return
        }
    }

    // 根据路径的节点解析出可绘制的路径
    parsePath() {
        // 每个格子中心的坐标
        let r = this.results
        for (let i = 0; i < r.length; i++) {
            let x = r[i].x + 0.5 * this.grid.blockSide
            let y = r[i].y + 0.5 * this.grid.blockSide
            let point = cc.v2(x, y)
            this.pathPoint.push(point)
        }
        // console.log(this.pathPoint)
    }

    // 根据当前的块，返回邻近四方向的块
    getNeighborBlock(block) {
        let neighbor = []
        let grid = this.grid.blockToGrid(block)
        if (grid.x > 0) {
            neighbor.push(cc.v2(grid.x - 1, grid.y))
        }
        if (grid.x < this.grid.row - 1) {
            neighbor.push(cc.v2(grid.x + 1, grid.y))
        }
        if (grid.y > 0) {
            neighbor.push(cc.v2(grid.x, grid.y - 1))
        }
        if (grid.y < this.grid.col - 1) {
            neighbor.push(cc.v2(grid.x, grid.y + 1))
        }

        let blocks = []
        for (let i = 0; i < neighbor.length; i++) {
            blocks.push(this.grid.gridToBlock(neighbor[i]))
        }
        // console.log('grid',grid,neighbor)
        // console.log(blocks)
        // 去掉墙体和已经在关闭列表的
        blocks = blocks.filter(i => !i.isClose).filter(i => !i.isWall)//.filter(i => !i.isOpen) // openr仍需要后续检查
        // 去除起点和终点
        blocks = blocks.filter(i => this.checkType(i) != startGrid)//.filter(i => this.checkType(i) != endGrid)
        return blocks
    }

    // 对邻近节点的处理
    neighborProcess(neighborArray, currentBlock) {
        let newOpenBlock = []
        for (let i = 0; i < neighborArray.length; i++) {
            let block = neighborArray[i]

            if (!block.isOpen) {
                block.parent = currentBlock
                this.computeFGH(block)
                if (this.checkType(block) != startGrid
                    && !block.isClose
                    && !block.isWall
                    && this.checkType(block) != endGrid) {

                    this.grid.changeToOpen(block)
                }
                newOpenBlock.push(block)

            } else if (block.G < currentBlock.G) {
                // console.log(block, currentBlock)
                block.parent = currentBlock
                this.computeFGH(block)
            }

        }
        return newOpenBlock
    }

    // 计算块的fgh值
    computeFGH(block) {
        let grid = this.grid.blockToGrid(block)
        let start = this.grid.startBlock
        let end = this.grid.endBlock
        // block.G = Math.abs(start.x - grid.x) + Math.abs(start.y - grid.y)
        block.G = block.parent ? block.parent.G + 1 : 0 // G值应该是父节点的G值+1， 上面算出来仍然是估计值，所以错了
        block.H = Math.abs(end.x - grid.x) + Math.abs(end.y - grid.y)
        block.F = block.G + block.H
        block.grid = this.grid.blockToGrid(block)
    }

}

export default Search